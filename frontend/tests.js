var assert = require('assert');

/* To run tests: mocha --require babel-register --compilers js:babel-core/register tests.js */

import { getSearchStatesPage }  from './src/queries/stateQueries';
import { getSearchPantriesPage } from './src/queries/pantryQueries';
import { getSearchOpportunitiesPage } from './src/queries/volunteerQueries';

import {getStateInfo, getVolunteerInfo, getFoodpantriesInfo} from './src/queries/InstanceQueries';

/* Written by Sergio */
describe("Check if model page returns the right number of instances: ", function() {
    it("State Page returns 16 instances: ", async function() {
        const data =  await getSearchStatesPage(1, '', '', '', '');
        assert.equal(data.results.length , 16);
    });

    it("Food Pantries Page returns 16 instances: ", async function() {
        const data =  await getSearchPantriesPage(1, '', '', '', '');
        assert.equal(data.results.length , 16);
    });

    it("Volunteer Opportunities Page returns 16 instances: ", async function() {
        const data =  await getSearchOpportunitiesPage(1, '', '', '', '');
        assert.equal(data.results.length , 16);
    });
});

/* Written by Sergio */
describe("Check if model page returns correct number of TOTAL instances: ", function() {
    it("State Page returns a correct count of instances: ", async function() {
        const data =  await getSearchStatesPage(1, '', '', '', '');
        assert.equal(data.count , 51);
    });

    it("Food Pantries Page returns a correct count of instances: ", async function() {
        const data =  await getSearchPantriesPage(1, '', '', '', '');
        assert.equal(data.count, 31938);
    });

    it("Volunteer Opportunities Page returns a correct count of instances: ", async function() {
        const data =  await getSearchOpportunitiesPage(1, '', '', '', '');
        assert.equal(data.count, 10143);
    });
});

/* Written by Sergio */
describe("Check if States can be searched properly and check their attributes : ", function() {
    it("Alabama's poverty rate is set properly: ", async function() {
        const data =  await getSearchStatesPage(1, 'Alabama', '', '', '');
        assert.equal(data.results[0].poverty , 17.1);
    });

    it("Alaska's poverty rate is set properly: ", async function() {
        const data =  await getSearchStatesPage(1, 'Alaska', '', '', '');
        assert.equal(data.results[0].poverty , 9.9);
    });

    it("Arizona's poverty rate is set properly: ", async function() {
        const data =  await getSearchStatesPage(1, 'Arizona', '', '', '');
        assert.equal(data.results[0].poverty , 16.4);
    });
});

/* Written by Sergio */
describe("Check if Food Pantries can be searched properly and check their attributes: ", function() {
    it("Check if 'Abbott Loop Community Church' State variable is set properly: ", async function() {
        const data =  await getSearchPantriesPage(1, 'Abbott Loop Community Church', '', '', '');
        assert.equal(data.results[0].state[0], "Alaska");
    });

    it("Check if 'Anchorage City Church - Food Shelf' State variable is set properly: ", async function() {
        const data =  await getSearchPantriesPage(1, 'Anchorage City Church - Food Shelf', '', '', '');
        assert.equal(data.results[0].state[0], "Alaska");
    });

    it("Check if 'Fellowship In Serving Humanity' State variable is set properly: ", async function() {
        const data =  await getSearchPantriesPage(1, 'Fellowship In Serving Humanity', '', '', '');
        assert.equal(data.results[0].state[0], "Alaska");
    });
});

/* Written by Sergio */
describe("Check if Volunteer Opportunities can be searched properly and check their attributes: ", function() {
    it("Check if volunteer opportunity 'Open opportunities at Tu Nidito Children and Family Services' State variable is set properly: ", async function() {
        const data =  await getSearchOpportunitiesPage(1, 'Open opportunities at Tu Nidito Children and Family Services', '', '', '');
        assert.equal(data.results[0].state[0], "Arizona");
    });

    it("Check if volunteer opportunity 'Winter Horse Camp Barn Director' State variable is set properly: ", async function() {
        const data =  await getSearchOpportunitiesPage(1, 'Winter Horse Camp Barn Director', '', '', '');
        assert.equal(data.results[0].state[0], "Texas");
    });

    it("Check if volunteer opportunity 'Volunteer Opportunities at The Captain Frederick Pabst Mansion' State variable is set properly ", async function() {
        const data =  await getSearchOpportunitiesPage(1, 'Volunteer Opportunities at The Captain Frederick Pabst Mansion', '', '', '');
        assert.equal(data.results[0].state[0], "Wisconsin");
    });
});

/* Written by Sergio */
describe("Check if State filtering and sorting works properly: ", function() {
    it("State Sorting by Name, Alphabetically A-Z: ", async function() {
        const data =  await getSearchStatesPage(1, '', '', '', 'alphaASC');
        assert.equal(data.results[0].name, "Alabama");
    });

    it("State Sorting by Name, Alphabetically Z-A: ", async function() {
        const data =  await getSearchStatesPage(1, '', '', '', 'alphaDESC');
        assert.equal(data.results[0].name, "Wyoming");
    });

    it("State Sorting by Poverty Rate, Ascending", async function() {
        const data =  await getSearchStatesPage(1, '', '', '', 'povertyASC');
        assert.equal(data.results[0].name, "New Hampshire");
    });

    it("State Sorting by Poverty Rate, Descending", async function() {
        const data =  await getSearchStatesPage(1, '', '', '', 'povertyDESC');
        assert.equal(data.results[0].name, "Louisiana");
    });

    it("Filter by Region, Northeast", async function() {
        const data =  await getSearchStatesPage(1, '', '', 'Northeast', '');
        assert.equal(data.results[0].name, "Massachusetts");
    });

    it("Filter by Region, Midwest", async function() {
        const data =  await getSearchStatesPage(1, '', '', 'Midwest', '');
        assert.equal(data.results[0].name, "Ohio");
    });

    it("Filter by Region, South", async function() {
        const data =  await getSearchStatesPage(1, '', '', 'South', '');
        assert.equal(data.results[0].name, "District of Columbia");
    });

    it("Filter by Region, West", async function() {
        const data =  await getSearchStatesPage(1, '', '', 'West', '');
        assert.equal(data.results[0].name, "Nevada");
    });

    it("Filter by Poverty Rate: 1-25", async function() {
        const data =  await getSearchStatesPage(1, '', '25', '', '');
        assert.equal(data.results[0].name, "Massachusetts");
    });

    it("Filter by Poverty Rate: 26-50", async function() {
        const data =  await getSearchStatesPage(1, '', '50', '', '');
        assert.equal(data.results[0].name, "Nevada");
    });

    it("Filter by Poverty Rate: 51-75", async function() {
        const data =  await getSearchStatesPage(1, '', '75', '', '');
        assert.equal(data.results[0].name, "Ohio");
    });

    it("Filter by Poverty Rate: 51-75", async function() {
        const data =  await getSearchStatesPage(1, '', '100', '', '');
        assert.equal(data.results[0].name, "Arkansas");
    });
});

//Test Made by Xi Jin
describe("Check if state info is retrieved correctly", function() {
    it("Check if state name matches the request name", async function() {
        const data =  await getStateInfo("Alabama");
        assert.equal(data.name , "Alabama");
    });
    it("Check if state name matches the request name", async function() {
        const data =  await getStateInfo("Texas");
        assert.equal(data.name , "Texas");
    });
    it("Check if state name matches the request name", async function() {
        const data =  await getStateInfo("New York");
        assert.equal(data.name , "New York");
    });
});

describe("Check if volunteer info is retrieved correctly", function() {
    it("Check if volounteer opportunity name matches the request name", async function() {
        const data =  await getVolunteerInfo("1");
        assert.equal(data.name , "Open opportunities at American Red Cross Alabama Region");
    });
    it("Check if volounteer opportunity city is correctly returned", async function() {
        const data =  await getVolunteerInfo("1");
        assert.equal(data.city , "Adamsville");
    });
    it("Check if volounteer opportunity address is correctly returned", async function() {
        const data =  await getVolunteerInfo("1");
        assert.equal(data.address , "300 Chase Park S, Hoover, AL 35244");
    });
});

describe("Check if food pantry info is retrieved correctly", function() {
    it("Check if volounteer opportunity name matches the request name", async function() {
        const data =  await getFoodpantriesInfo("123");
        assert.equal(data.name , "Loaves And Fishes");
    });
    it("Check if volounteer opportunity returns right state", async function() {
        const data =  await getFoodpantriesInfo("123");
        assert.equal(data.state[0], "Alabama");
    });
    it("Check if volounteer opportunity returns right address", async function() {
        const data =  await getFoodpantriesInfo("123");
        assert.equal(data.address , "337 Hatcher Street, AL");
    });
});