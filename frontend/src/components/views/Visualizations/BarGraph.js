import React from 'react';
import Axios from 'axios'
import * as d3 from "d3";
import "./BarGraph.css"

export default class BarGraph extends React.Component {
    constructor() {
        super();
        this.state = {
          povertyRates: []
        }
      }

      async componentWillMount() {
        const apiCall = "http://api.caringhands.me/allstates/?format=json";
        Axios.get(apiCall)
          .then(response => {
            const data = response.data;
            var povertyRates = [];
            this.parseStatePovertyRates(data, povertyRates);
            return povertyRates;
          })
          .then(data => {
            this.setState({povertyRates: data});
          })
      }

      parseStatePovertyRates(data, povertyRates) {
        for (var i = 0; i < data.length; i++) {
            var stateInfo = {}
            stateInfo["state"] = data[i].name;
            stateInfo["poverty"] = data[i].poverty;
            povertyRates.push(stateInfo)
        }
      }

      render() {
        const margin = 60;
        const width = 1300 - 2 * margin;
        const height = 600 - 2 * margin;
    
        const svg = d3.select('svg');

        const chart = svg.append('g')
            .attr('transform', `translate(${margin}, ${margin})`);

        const xScale = d3.scaleBand()
            .range([0, width])
            .domain(this.state.povertyRates.map((s) => s.state))
            .padding(0.4)

        const yScale = d3.scaleLinear()
            .range([height, 0])
            .domain([0, 20]);

        const makeYLines = () => d3.axisLeft()
            .scale(yScale)

        chart.append('g')
            .attr("class", "x axis")
            .attr('transform', `translate(0, ${height})`)
             .call(d3.axisBottom(xScale).ticks(10))
             .selectAll("text")	
               .style("text-anchor", "end")
               .attr("dx", "-.8em")
               .attr("dy", ".15em")
               .attr("transform", "rotate(-65)");
        
        chart.append('g')
            .call(d3.axisLeft(yScale));

        chart.append('g')
            .attr('class', 'grid')
            .call(makeYLines()
              .tickSize(-width, 0, 0)
              .tickFormat('')
            )

        const barGroups = chart.selectAll()
            .data(this.state.povertyRates)
            .enter()
            .append('g')

        barGroups
            .append('rect')
            .attr('class', 'bar')
            .attr('x', (g) => xScale(g.state))
            .attr('y', (g) => yScale(g.poverty))
            .attr('height', (g) => height - yScale(g.poverty))
            .attr('width', xScale.bandwidth())
            .on('mouseenter', function (actual, i) {
                d3.selectAll('.poverty')
                  .attr('opacity', 0)
        
                d3.select(this)
                  .transition()
                  .duration(300)
                  .attr('opacity', 0.6)
                  .attr('x', (a) => xScale(a.state) - 5)
                  .attr('width', xScale.bandwidth() + 10)
        
                const y = yScale(actual.poverty)
        
                chart.append('line')
                  .attr('id', 'limit')
                  .attr('x1', 0)
                  .attr('y1', y)
                  .attr('x2', width)
                  .attr('y2', y)
        
                barGroups.append('text')
                  .attr('class', 'divergence')
                  .attr('x', (a) => xScale(a.state) + xScale.bandwidth() / 2)
                  .attr('y', (a) => yScale(a.poverty) + 30)
                  .attr('fill', 'white')
                  .attr('text-anchor', 'middle')
                  .text((a, idx) => {
                    const divergence = (a.poverty - actual.poverty).toFixed(1)
                    
                    let text = ''
                    if (divergence > 0) text += '+'
                    text += `${divergence}%`
        
                    return idx !== i ? text : '';
                  })
        
              })
              .on('mouseleave', function () {
                d3.selectAll('.poverty')
                  .attr('opacity', 1)
        
                d3.select(this)
                  .transition()
                  .duration(300)
                  .attr('opacity', 1)
                  .attr('x', (a) => xScale(a.state))
                  .attr('width', xScale.bandwidth())
        
                chart.selectAll('#limit').remove()
                chart.selectAll('.divergence').remove()
              })

            barGroups 
              .append('text')
              .attr('class', 'poverty')
              .attr('x', (a) => xScale(a.state) + xScale.bandwidth() / 2)
              .attr('y', (a) => yScale(a.poverty) + 30)
              .attr('text-anchor', 'middle')
              .text((a) => `${a.poverty}%`)

            svg
              .append('text')
              .attr('class', 'label')
              .attr('x', -(height / 2) - margin)
              .attr('y', margin / 2.4)
              .attr('transform', 'rotate(-90)')
              .attr('text-anchor', 'middle')
              .text('Poverty Rate (%)')

            svg.append('text')
              .attr('class', 'label')
              .attr('x', width / 2 + margin)
              .attr('y', height + margin * 3.0)
              .attr('text-anchor', 'middle')
              .text('States')

            svg.append('text')
              .attr('class', 'title')
              .attr('x', width / 2 + margin)
              .attr('y', 40)
              .attr('text-anchor', 'middle')
              .text('Poverty Rates of States in the USA')
        
        return (
            <div className="graphLayout">
                <svg className="graphContainer"/>
            </div>
        );
  }
}