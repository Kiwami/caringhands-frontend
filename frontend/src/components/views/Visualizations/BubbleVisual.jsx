import React, { Component } from 'react';

import BubbleChart from '@weknow/react-bubble-chart-d3';
import { Container, Row, Col } from 'reactstrap';


class BubbleVisual extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: null,
      isLoaded: false,
      items: [],
      data: []
    };
  }

  componentDidMount() {
    fetch((`http://api.caringhands.me/visualization/?format=json`), {
          method: "GET",
          headers: {
              "Content-Type": "application/json",
          }
      })
      .then(res => {
        this.setState({
          status: res.status
        });
        return res.json();
      })
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result,
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
    )
  }

  render() {
    var items = this.state.items;
    var results_dict = {};
    for(var i=0; i<items.length; i++){
      var abrv = items[i].abrv.toUpperCase();
      results_dict[abrv] = items[i].volunteer + items[i].foodpantry;
    }
    var data = []
    // for (var key in dict){
    //   data.push({ label: 'TX', value: 1}); 
    // }
    Object.keys(results_dict).forEach(function(key) {
      data.push({ label: key, value: results_dict[key]}); 
   });
  
    

    return (

      <Container>
        <Row>
            <Col xs="4" md={{size: 100, offset: 1000}}>

            </Col>
            <Col xs="auto" md={{size: 100, offset: 1000}}>
            <div className="BubbleVisual">
              <h1>
                Total Number of Food Pantries and Opportunities per State
              </h1>
              <br />
              <BubbleChart
                graph= {{
                  zoom: 5,
                  offsetX: -0.05,
                  offsetY: -0.01,
                }}
                width={180}
                height={180}
                showLegend={false} // optional value, pass false to disable the legend.
                legendPercentage={20} // number that represent the % of with that legend going to use.
                legendFont={{
                      family: 'Arial',
                      size: 12,
                      color: '#000',
                      weight: 'bold',
                    }}
                valueFont={{
                      family: 'Arial',
                      size: 12,
                      color: '#fff',
                      weight: 'bold',
                    }}
                labelFont={{
                      family: 'Arial',
                      size: 10,
                      color: '#fff',
                      weight: 'bold',
                    }}
                data={data}
              />

            </div>
            </Col>
            <Col xs="4" md={{size: 100, offset: 1000}}>
                
            </Col>
          
        </Row>
      </Container>
      

    );
  }
}

export default BubbleVisual;