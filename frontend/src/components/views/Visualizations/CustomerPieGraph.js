import React from 'react';
import * as d3 from "d3";
import { PieChart } from 'react-d3-components'
import { Container, Row, Col } from 'reactstrap';


export default class CustomerPieGraph extends React.Component {

      render() {
 
          var data = {
                  label: 'Texas Demographics',
                  values: [{x: 'White 75%', y: 75}, {x: 'Black 12%', y: 12}, {x: 'Asian 4%', y: 4},
                  {x: 'Other 5%', y: 9}]
          };
           
          var sort = d3.descending // d3.ascending, d3.descending, func(a,b) { return a - b; }, etc...
            
        return (
            <Container>
                <Row>
                    <div style={{background: "rgb(96, 134, 184)", width: 1000}}>
                        <Col xs="4" md={{size: 100, offset: 1000}}>
        
                        </Col>
                        <Col xs="auto" md={{size: 100, offset: 1000}}>
                        <h1> Texas Demographics </h1>
                        <PieChart
                            data={data}
                            width={1000}
                            height={500}
                            margin={{top: 10, bottom: 10, left: 100, right: 100}}
                            sort={sort}
                        />
                        </Col>
                        <Col xs="4" md={{size: 100, offset: 1000}}>
                            
                        </Col>
                    </div>
                </Row>
            </Container>
        );
  }
}