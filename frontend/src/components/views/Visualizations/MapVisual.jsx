import React, {Component} from 'react';
import * as d3 from "d3";
import uStates from './uStates';
import './MapVisual.css';

class MapVisual extends Component {
  constructor(props) {
    super(props);
    this.state = {
      status: null,
      isLoaded: false,
      items: []
    };
    this.drawChart = this.drawChart.bind(this);
  }

  componentDidMount() {
    fetch((`http://api.caringhands.me/visualization/?format=json`), {
          method: "GET",
          headers: {
              "Content-Type": "application/json",
          }
      })
      .then(res => {
        this.setState({
          status: res.status
        });
        return res.json();
      })
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            items: result,
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
    )
  }

  drawChart() {
    function tooltipHtml(n, d){	/* function to create html content string in tooltip div. */
      return "<h4>"+n+"</h4><table>"+
        "<tr><td>Food pantries</td><td>"+(d.foodpantries)+"</td></tr>"+
        "<tr><td>Volunteer:</td><td>"+(d.volunteer)+"</td></tr>"+
        "</table>";
    }

    var items = this.state.items;
    var volunteer_dict = {};
    var food_dict = {};
    for(var i=0; i<items.length; i++){
      var abrv = items[i].abrv.toUpperCase();
      volunteer_dict[abrv] = items[i].volunteer;
      food_dict[abrv] = items[i].foodpantry;
    }

    var sampleData ={};	
    ["HI", "AK", "FL", "SC", "GA", "AL", "NC", "TN", "RI", "CT", "MA",
    "ME", "NH", "VT", "NY", "NJ", "PA", "DE", "MD", "WV", "KY", "OH", 
    "MI", "WY", "MT", "ID", "WA", "DC", "TX", "CA", "AZ", "NV", "UT", 
    "CO", "NM", "OR", "ND", "SD", "NE", "IA", "MS", "IN", "IL", "MN", 
    "WI", "MO", "AR", "OK", "KS", "LA", "VA"]
      .forEach(function(d){ 
        var f = food_dict[d];
        var v = volunteer_dict[d];
        
        sampleData[d]={foodpantries: f, volunteer: v, 
                       color:d3.interpolate("#ffffcc", "#ff9d00")((f+v)/1500)}; 
      });
    
    /* draw states on id #statesvg */	
    uStates.draw("#statesvg", sampleData, tooltipHtml);
    
    d3.select(window.frameElement).style("height", "600px"); 

  }
  render() {
    if(this.state.isLoaded){
      this.drawChart();
    }
    return (
      <div className="container">
        <div id="tooltip"></div>
        <svg width="960" height="600" id="statesvg"></svg>
      </div>
    );
    
  }
}

export default MapVisual;
