import React, { Component } from 'react';
import Axios from 'axios';
import { Container, Row, Col } from 'reactstrap';


import BubbleChart from '@weknow/react-bubble-chart-d3';

class CustomerBubbleVisual extends Component {
  constructor(props) {
    super(props);
    this.state = {
      isLoaded: false,
      items: [],
    };
  }

  componentDidMount() {
    this.reset();
  }

  reset() {
    Axios.get("http://api.castmyvote.me/bubble/")
      .then(response => response.data)
      .then(data => this.setState({ 
        items: data,
        isLoaded: true
      }));
  }

  render() { 
    var results_dict = {};
    var visual_data = []
    {this.state.items.map(dynamicLocation => 
      results_dict[dynamicLocation.name] = dynamicLocation.population
    )}
    Object.keys(results_dict).forEach(function(key) {
      visual_data.push({ label: key, value: Math.round(results_dict[key].replace(/,/g, '') / 10000)}); 
    });
    return (
      <Container>
      <Row>
          <Col xs="4" md={{size: 100, offset: 1000}}>

          </Col>
          <Col xs="auto" md={{size: 100, offset: 1000}}>
          <div className="BubbleVisual">
          <h1>
            Population per Congressional District in tens of thousands
          </h1>
          <br />
          <BubbleChart
            graph= {{
              zoom: 5,
              offsetX: -0.05,
              offsetY: -0.01,
            }}
            width={180}
            height={180}
            showLegend={false} // optional value, pass false to disable the legend.
            legendPercentage={20} // number that represent the % of with that legend going to use.
            legendFont={{
                    family: 'Arial',
                  size: 12,
                  color: '#000',
                  weight: 'bold',
                }}
            valueFont={{
                  family: 'Arial',
                  size: 12,
                  color: '#fff',
                  weight: 'bold',
                }}
            labelFont={{
                  family: 'Arial',
                  size: 10,
                  color: '#fff',
                  weight: 'bold',
                }}
            data={visual_data}
          />

          </div>
          </Col>
          <Col xs="4" md={{size: 100, offset: 1000}}>
              
          </Col>
        
      </Row>
    </Container>
    );
  }
}

export default CustomerBubbleVisual;