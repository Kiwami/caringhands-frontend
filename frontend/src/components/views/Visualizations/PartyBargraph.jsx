import React, {Component} from 'react';
import { BarChart } from "react-d3-components";
import { Container, Row, Col } from 'reactstrap';


const party = [
    {x: "Libertarian", y: 76},
    {x: "Independent", y: 10},
    {x: "Republican", y: 179},
    {x: "Democrat", y: 199}
]


var data = [{
    label: 'somethingA',
    values: party
}];

class PartyBargraph extends Component {
    render() {
        return (
            <Container>
                <Row>
                    <div style={{background: "rgb(96, 134, 184)", width: 1000} }>
                    <Col xs="4" md={{size: 100, offset: 1000}}>

                    </Col>
                    <Col xs="auto" md={{size: 100, offset: 1000}}>
                    <h1> Number of Candidates in Each Party </h1>
                        <BarChart
                            data={data}
                            width={1000}
                            height={500}
                            margin={{top: 10, bottom: 50, left: 100 , right: 10}}/>
                    </Col>
                    <Col xs="4" md={{size: 100, offset: 1000}}>
                    
                    </Col>
                    </div>
                </Row>
            </Container>
        );
    }
}
export default PartyBargraph