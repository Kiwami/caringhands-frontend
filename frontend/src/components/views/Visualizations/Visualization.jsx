import React, {Component} from 'react';
import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col } from 'reactstrap';
import classnames from 'classnames';
import MyNavBar from '../../MyNavBar';
import MapVisual from './MapVisual';
import BarGraph from './BarGraph';
import BubbleVisual from './BubbleVisual';
import CustomerBubbleVisual from './CustomerBubbleVisual'
import PartyBargraph from './PartyBargraph';
import CustomerPieGraph from './CustomerPieGraph';


class Visualization extends Component {
    constructor(props) {
        super(props);
    
        this.toggle = this.toggle.bind(this);
        this.state = {
          activeTab: '1'
        };
      }
    
      toggle(tab) {
        if (this.state.activeTab !== tab) {
          this.setState({
            activeTab: tab
          });
        }
      }
      render() {
        return (
          <div>
            <MyNavBar />
            <Nav tabs>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '1' })}
                  onClick={() => { this.toggle('1'); }}
                >
                  Bar Graph
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '2' })}
                  onClick={() => { this.toggle('2'); }}
                >
                  Map
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '3' })}
                  onClick={() => { this.toggle('3'); }}
                >
                  Bubble Chart
                </NavLink>
              </NavItem>
              <NavItem>
                <NavLink
                  className={classnames({ active: this.state.activeTab === '4' })}
                  onClick={() => { this.toggle('4'); }}
                >
                  Customer Visualization
                </NavLink>
              </NavItem>
            </Nav>
            <TabContent activeTab={this.state.activeTab}>
              <TabPane tabId="1">
                <Row>
                  <Col sm="12">
                    <br />
                    
                    <BarGraph />
                  </Col>
                </Row>
              </TabPane>
              <TabPane tabId="2">
                <Col sm="12">
                    <br />
                    <h2>Food pantries and volunteer in each states</h2>
                    <MapVisual />
                </Col>
              </TabPane>
              <TabPane tabId="3">
                <Col sm="12">
                    <br />
                    <BubbleVisual/>
                </Col>
              </TabPane>
              <TabPane tabId="4">
                <Col sm="12">
                    <br />
                    <CustomerBubbleVisual/>
                    <br />
                    <PartyBargraph/>
                    <br />
                    <CustomerPieGraph/>
                </Col>
              </TabPane>
            </TabContent>
          </div>
        );
      }
}
export default Visualization;