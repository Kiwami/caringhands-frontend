import React, { Component } from 'react';

import 'bootstrap/dist/css/bootstrap.min.css';

//Components
import { Container, Row, Col } from 'reactstrap';
import MyNavBar from '../../MyNavBar'
import MainDescription from '../InstanceComponents/MainDescription'
import FsideInfo from '../InstanceComponents/FsideInfo'
import Opportunity from '../InstanceComponents/Opportunity'

class FoodInstance extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      status: 0,
      item: null
    };
  }
  componentDidMount() {
    var id = this.props.match.params.id
    fetch((`http://api.caringhands.me/foodpantries/${id}/?format=json`), {
      method: "GET",
      headers: {
          "Content-Type": "application/json",
      }
    }) 
    .then(res => {
        this.setState({
          status: res.status
        });
        return res.json();
      })
      .then(
        (result) => {
          this.setState({
            isLoaded: true,
            item: result,
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          this.setState({
            isLoaded: true,
            error
          });
        }
      )
  }
  render() {
    var { error, isLoaded, status, item } = this.state

    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded){
        return <div>Loading...</div>
    }else if (status !== 200){
        return <h1>404, page not found</h1>
    }else{
        return (
          <div>
            <MyNavBar/>
            <Container>
              <Row>
                <Col sm="9">
                  <MainDescription 
                    name={item.name}
                    description={item.description}
                    img={item.main_img}
                  />
                  <hr />
                  {console.log(item.state[0])}
                  <Opportunity title={item.state[0]+" Opportunities"} param={item.state[0]+"/opportunities"} type={1}/>
                </Col>
                <Col sm="3">
                  <FsideInfo
                    logo={item.logo_img}
                    link={item.website}
                    location={item.address}
                    phone={item.phone}
                    state={item.state[0]}
                    pos={item.state_pos_img} 
                  />
                </Col>
              </Row>
            </Container>
          </div>
        );
    }
  }
}

export default FoodInstance;