import React from 'react';
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle} from 'reactstrap';
import './AboutMeCard.css'

class AboutMe extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
        count: 0
    }
}
  
  render() {
    
    return (
      <div className="AboutMeCard">
        <Card style={{height: 500}}>
          <CardImg className = "AboutMeImage" top width="100%" src={this.props.img} style={{width: 245, height: 310}}/>
          <CardBody>
            <CardText>{this.props.responsibilities}</CardText>
            <CardTitle > {this.props.name} </CardTitle>
            <CardSubtitle>{this.props.description}</CardSubtitle>
          </CardBody>
        </Card>
      </div>
    );
  }
}

export default AboutMe;