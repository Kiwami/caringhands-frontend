import React from 'react';
import './Data.css'
import {
    Nav,
    NavItem, 
    NavLink
  } from 'reactstrap';

class Data extends React.Component {
    render() {
      return (
        <div className= "Data">
            <h2>Data</h2>
            <Nav className= "Data">
                <NavItem>
                    <NavLink href="https://www.wikipedia.org">Wikipedia</NavLink>
                </NavItem>
            </Nav>
            <p> Used to obtain state statistics relating to poverty. The Wikipedia REST API returned an XML page, and an HTML parser script had to be written in order to extra the specific information that was needed </p>
            <Nav className= "Data">
                <NavItem>
                    <NavLink href="https://www.foodpantries.org">Food Pantries</NavLink>
                </NavItem>
            </Nav>
            <Nav className= "Data">
                <NavItem>
                    <NavLink href="www.data.com">data.com</NavLink>
                </NavItem>
            </Nav>
            <p> Both used to obtain information about food pantries in the US. Websites were scrapped for food pantries in each city for every state in the US. Their REST API returned an XML response and another scraper was also written to extract the information needed</p>
            <Nav className= "Data">
                <NavItem>
                    <NavLink href="https://www.givepulse.com">Give Pulse</NavLink>
                </NavItem>
            </Nav>
            <p> Used to obtain volunteer opportunities in the US. The support team at Give Pulse had to be contacted to obtain an API key in order to be able to query their endpoint. This source was also a REST API. </p>
        </div>
      );
    }
  }
  
  export default Data;