import React from 'react';
import './Description.css'

class Description extends React.Component {
    render() {
      return (
        <div className= "Description">
            <h1> About Us </h1>
            <p>
            Our goal is to advocate a strong focus on civic engagement through allowing individuals the opportunity to match 
            with an organization that will allow them to donate their own time and resources to help those in need primarily 
            through the provision of basic necessitates such as food and volunteering.
            </p>
            <h1> Data Integration </h1>
            <p>
            State data will contain homelessness statistics, data on large organizations dedicated to helping those in need, and information
            on any legisliation that was made to alleviate poverty. We will organize food pantries within the US by state as well as link users
            to information on what services they provide and ways to get in Volunteering oppurtunities will provide listing of local
            oppurtunities, their specifications, and the potential community impact for that state.
            </p>
            <h1> Our Team </h1>
        </div>
      );
    }
  }
  
  export default Description;