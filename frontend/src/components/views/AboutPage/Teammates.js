import React, {Component} from 'react'
import AboutMeCard from "./AboutMeCard"
import {CardDeck} from 'reactstrap'
import './Teammates.css'
import Julia from "../../../data/aboutMePictures/julia.jpeg"
import Harry from "../../../data/aboutMePictures/harry.jpeg"
import Xi from "../../../data/aboutMePictures/xi.jpeg"
import Andrei from "../../../data/aboutMePictures/andrei.jpeg"
import Sergio from "../../../data/aboutMePictures/sergio.jpeg"

class Teammates extends Component {

    render() {
        return (
            <div className="Teammates">
                <div className="container">
                <CardDeck className= "CardDeck">
                    <AboutMeCard name= 'Xi Jin' img={Xi} alt='' description= 'Senior computer science student who loves to work with other people instead of working alone'
                                responsibilities= 'Frontend Engineer'/>
                    <AboutMeCard name= 'Julia Hoang' img={Julia} alt='' description= 'Senior computer science student at UT who loves tennis, cats and clean drinking water'
                                responsibilities= 'Frontend Engineer'/>
                    <AboutMeCard name= 'Sergio Moreno' img={Sergio} alt='' description= 'Senior computer science student who likes playing way too many video games'
                                responsibilities= 'Frontend Engineer'/>
                </CardDeck>
                <CardDeck className= "CardDeck">
                    <AboutMeCard name= 'Andrei Jaume' img={Andrei} alt='' description= 'Senior computer science major from Mexico who loves fencing and outdoor sports'
                                responsibilities= 'Backend Engineer'/>
                    <AboutMeCard name= 'Lingfei He' img={Harry} alt='' description= 'Senior computer science major, exchange at UT who likes basketball and video games'
                                responsibilities= 'Backend Engineer'/>
                </CardDeck> 
                </div>
            </div>
       
        );
    }
}
export default Teammates;
