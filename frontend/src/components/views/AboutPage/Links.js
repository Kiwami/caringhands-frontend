import React from 'react';
import './Links.css'
import {
    Nav,
    NavItem, 
    NavLink
  } from 'reactstrap';

class Links extends React.Component {
    render() {
      return (
        <div className= "Links">
            <h2>Links</h2>
            <Nav className= "Links">
                <NavItem>
                    <NavLink href="https://documenter.getpostman.com/view/5473192/RzZ3LhdP">Postman API</NavLink>
                </NavItem>
            </Nav>
            <Nav className= "Links">
                <NavItem>
                    <NavLink href="https://gitlab.com/andrei.jaumewillenska/caringhands.git">GitLab Repo</NavLink>
                </NavItem>
            </Nav>
        </div>
      );
    }
  }
  
  export default Links;