import React from 'react';
import { Table } from 'reactstrap';
import Axios from 'axios'

export default class StatsTable extends React.Component {
    constructor() {
        super();
        this.state = {
          backend_commits: [],
          backend_issues: [],
          frontend_commits: [],
          frontend_issues: []
        }
      }

      async componentWillMount() {
        const commitsBackendURL = "https://gitlab.com/api/v4/projects/8573582/repository/contributors?sort=desc";
        Axios.get(commitsBackendURL)
          .then(response => {
            const data = response.data;
            var commits = [0,0,0,0,0,0];
            this.parseGitLabCommits(data, commits);
            return commits;
          })
          .then(data => {
            this.setState({backend_commits: data});
          })

          const commitsFrontendURL = "https://gitlab.com/api/v4/projects/8834016/repository/contributors?sort=desc";
          Axios.get(commitsFrontendURL)
            .then(response => {
              const data = response.data;
              var commits = [0,0,0,0,0,0];
              this.parseGitLabCommits(data, commits);
              return commits;
            })
            .then(data => {
              this.setState({frontend_commits: data});
            })

        const issuesBackendURL = "https://gitlab.com/api/v4/projects/8573582/issues?per_page=100";
        Axios.get(issuesBackendURL)
            .then(response => {
                const data = response.data;
                var issues = [0,0,0,0,0,0];
                this.parseGitLabIssues(data,issues);
                return issues
            })
            .then(data => {
            this.setState({backend_issues: data});
            })
      }

      parseGitLabIssues(data, issues) {
          for (var i = 0; i < data.length; i++) {
              switch(data[i].author.id) {
                case 2764274:
                    issues[0] += 1;
                    issues[5] += 1;
                    break;
                case 2763649:
                    issues[1] += 1;
                    issues[5] += 1;
                    break;
                case 2773366:
                    issues[2] += 1;
                    issues[5] += 1;
                    break;
                case 2763887:
                    issues[3] += 1;
                    issues[5] += 1;
                    break;
                case 2787388:
                    issues[4] += 1;
                    issues[5] += 1;
                    break;
                default:
                    break;
              }
          }
      }

      parseGitLabCommits(data, commits) {
          console.log(data.length)
          for (var i = 0; i < data.length; i++) {
              switch(data[i].name) {
                    case "Andrei Jaume":
                        commits[3] += data[i].commits;
                        commits[5] += data[i].commits;
                        break;
                    case "Sergio Moreno":
                        commits[2] += data[i].commits;
                        commits[5] += data[i].commits;
                        break;
                    case "Xi Jin":
                    case "jinxi97":
                        commits[0] += data[i].commits;
                        commits[5] += data[i].commits;
                        break;
                    case "Julia Hoang":
                        commits[1] += data[i].commits;
                        commits[5] += data[i].commits;
                        break;
                    case "lingfei He":
                        commits[4] += data[i].commits;
                        commits[5] += data[i].commits;
                        break;
                    default:
                        break;
              }
          }
      }

    render() {
        return (
        <Table striped>
            <thead>
            <tr>
                <th>Name</th>
                <th>Commits</th>
                <th>Issues</th>
                <th>Unit Tests</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <th scope="row">Xi Jin</th>
                <td>{this.state.backend_commits[0] + this.state.frontend_commits[0]}</td>
                <td>{this.state.backend_issues[0]}</td>
                <td>9</td>
            </tr>
            <tr>
                <th scope="row">Julia Hoang</th>
                <td>{this.state.backend_commits[1] + this.state.frontend_commits[1]}</td>
                <td>{this.state.backend_issues[1]}</td>
                <td>35</td>
            </tr>
            <tr>
                <th scope="row">Sergio Moreno</th>
                <td>{this.state.backend_commits[2] + this.state.frontend_commits[2]}</td>
                <td>{this.state.backend_issues[2]}</td>
                <td>42</td>
            </tr>
            <tr>
                <th scope="row">Andrei Jaume</th>
                <td>{this.state.backend_commits[3]}</td>
                <td>{this.state.backend_issues[3]}</td>
                <td>45</td>
            </tr>
            <tr>
                <th scope="row">Lingfei Hei</th>
                <td>{this.state.backend_commits[4]}</td>
                <td>{this.state.backend_issues[4]}</td>
                <td>132</td>
            </tr>
            <tr>
                <th scope="row">Total</th>
                <td>{this.state.backend_commits[5] + this.state.frontend_commits[5]}</td>
                <td>{this.state.backend_issues[5]}</td>
                <td>263</td>
            </tr>
            </tbody>
        </Table>
        );
  }
}