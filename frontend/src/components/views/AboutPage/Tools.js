import React from 'react';
import './Tools.css'

class Tools extends React.Component {
    render() {
      return (
        <div className= "Tools">
            <h2>Tools</h2>
            <p> AWS S3: To host our website </p>
            <p> GitLab: For collaboration and code storage </p>
            <p> React: To design our website </p>
            <p> Postman: For testing RESTful API </p>
            <p> Namescheap: To obtain free website domain (.me) </p>
        </div>
      );
    }
  }
  
  export default Tools;