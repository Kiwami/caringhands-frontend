import React, { Component } from 'react';
import MyNavBar from '../../MyNavBar'
import Description from './Description'
import Stats from './Stats'
import Data from './Data'
import Tools from './Tools'
import Links from './Links'
import Footer from '../../MyFooter'
import Teammates from './Teammates'

class About extends Component {

  render() {

    return (
      <div className="About">
        <MyNavBar />
        <div className="container">
          <Description />
          <Teammates />
          <Stats />
          <Data />
          <Tools />
          <Links />
        </div>
          <Footer />
      </div>
    );
  }
}

export default About;