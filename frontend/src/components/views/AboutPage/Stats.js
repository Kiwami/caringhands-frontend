import React from 'react';
import './Stats.css'
import StatsTable from './StatsTable'

class Stats extends React.Component {
    render() {
      return (
        <div>
            <h2 className= "Header"> Stats</h2>
            < StatsTable className= "Stats"/>
        </div>
      );
    }
  }
  
  export default Stats;