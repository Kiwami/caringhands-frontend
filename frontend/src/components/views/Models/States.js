import React, { Component } from 'react';
import {Container, Row, Col} from 'reactstrap';
import './Model.css';

/* Layout components */
import MyNavBar from '../../MyNavBar'
import MyCard   from '../../MyCard'

/* Pagination components */
import Pagination from "react-js-pagination";
import { getSearchStatesPage } from '../../../queries/stateQueries';

/* Filter and Search Components */
import Search from '../../Search'

class States extends Component {
  constructor(props) {
    super(props);
    this.state = {
      states: [],
      numStates: 0,
      activePage: 1,
      searchEntry: '',
      povertyFilter: '',
      regionFilter: '',
      sortFilter: ''
    };
    this.handlePageChange = this.handlePageChange.bind(this);
    this.updateSearch = this.updateSearch.bind(this);
    this.updateSort = this.updateSort.bind(this);
    this.updateRegion = this.updateRegion.bind(this);
    this.updatePoverty = this.updatePoverty.bind(this);
    this.reset = this.reset.bind(this);
  }

  async componentDidMount() {
    const data = await getSearchStatesPage(this.state.activePage, this.state.searchEntry, this.state.povertyFilter, this.state.regionFilter, this.sortFilter);
    this.setState({states: data.results, numStates: data.count});
  }

  async handlePageChange(pageNumber) {
    const data = await getSearchStatesPage(pageNumber, this.state.searchEntry, this.state.povertyFilter, this.state.regionFilter, this.state.sortFilter);
    this.setState({states: data.results, activePage: pageNumber});
    window.scrollTo(0, 0)
  }

  async updateSearch(entry) {
    const data = await getSearchStatesPage(1, entry, this.state.povertyFilter, this.state.regionFilter, this.state.sortFilter);
    this.setState({states: data.results, numStates: data.count, activePage: 1, searchEntry: entry});
    window.scrollTo(0, 0)
  }

  async updateSort(sort) {
    const data = await getSearchStatesPage(1, this.state.searchEntry, this.state.povertyFilter, this.state.regionFilter, sort);
    this.setState({states: data.results, numStates: data.count, activePage: 1, sortFilter: sort});
    window.scrollTo(0, 0)      
  }

  async updateRegion(region) {
    const data = await getSearchStatesPage(1, this.state.searchEntry, this.state.povertyFilter, region, this.state.sortFilter);
    this.setState({states: data.results, numStates: data.count, activePage: 1, regionFilter: region});
    window.scrollTo(0, 0)
  }

  async updatePoverty(poverty) {
    const data = await getSearchStatesPage(1, this.state.searchEntry, poverty, this.state.regionFilter, this.state.sortFilter);
    this.setState({states: data.results, numStates: data.count, activePage: 1, povertyFilter: poverty});
    window.scrollTo(0, 0)
  }

  async reset() {
    const data = await getSearchStatesPage(1, '', '', '', '');
    this.setState({states: data.results, numStates: data.count, activePage: 1, searchEntry: '', sortFilter: '', regionFilter: '', povertyFilter: ''});
    window.scrollTo(0, 0)
  }

  options = [["Name: A-Z", "Name: Z-A", "Poverty Rate: Ascending", "Poverty Rate: Descending"], ["Northeast", "Midwest", "South", "West"], ["1-25 Percentile", "26-50 Percentile", "51-75 Percentile", "76-100 Percentile"]]
  
  dict = {"Name: A-Z": "alphaASC", "Name: Z-A": "alphaDESC", "Poverty Rate: Ascending": "povertyASC", "Poverty Rate: Descending": "povertDESC",
  "Northeast": "Northeast", "Midwest": "Midwest", "South": "South", "West":"West",
  "1-25 Percentile":"25", "26-50 Percentile": "50", "51-75 Percentile": "75", "76-100 Percentile":"100"}

  render() {
    return (
      <div className="States"> 
        <MyNavBar />
        <Search placeholder="Search for a specific State..." searchParam={["Sort", "Filter by Region", "Filter by Poverty Rate"]}
                options={this.options} 
                updateSearch={this.updateSearch} updateSort={this.updateSort} 
                updateRegion={this.updateRegion} updateThird={this.updatePoverty} 
                reset={this.reset}
                dict = {this.dict}/>
        <Container className = "modelContainer">
            <Row className = "modelRow">
                  {this.state.states.map(dynamicState => 
                    <Col sm="3" className = "modelCol">
                      <div key = {dynamicState.name}>
                        <MyCard 
                          title = {dynamicState.name} 
                          link = {"./states/" + dynamicState.name} 
                          image = {dynamicState.main_img}
                          searchEntry = {this.state.searchEntry}
                          text1 = {"Poverty rate: "+dynamicState.poverty}
                          text2 = {"Abrv: "+dynamicState.abrv}
                          text3 = {"Region: "+dynamicState.region}/>
                      </div>
                    </Col>
                  )}
            </Row>
        </Container>
        <Pagination className = "pagination"
          pageRangeDisplayed  = {10}
          activePage = {this.state.activePage}
          activeLinkClass = "active"
          itemsCountPerPage = {16}
          totalItemsCount = {this.state.numStates}
          onChange = {this.handlePageChange}
        />
      </div>
    );
  }
}

export default States;
