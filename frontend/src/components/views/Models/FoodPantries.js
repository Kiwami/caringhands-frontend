import React, { Component } from 'react';
import {Container, Row, Col} from 'reactstrap';
import './Model.css';

/* Layout components */
import MyNavBar from '../../MyNavBar'
import MyCard   from '../../MyCard'

/* Pagination components */
import Pagination from "react-js-pagination";
import { getSearchPantriesPage } from '../../../queries/pantryQueries';


/* Filter and Search Components */
import Search from '../../Search'

class FoodPantries extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pantries: [],
      numPantries: 0,
      activePage: 1,
      searchEntry: '',
      stateFilter: '',
      regionFilter: '',
      sortFilter: ''
    };
    this.handlePageChange = this.handlePageChange.bind(this);
    this.updateSearch = this.updateSearch.bind(this);
    this.updateSort = this.updateSort.bind(this);
    // this.updateRegion = this.updateRegion.bind(this);
    this.updateState = this.updateState.bind(this);
    this.reset = this.reset.bind(this);
  }

  async componentDidMount() {
    const data = await getSearchPantriesPage(this.state.activePage, this.state.searchEntry, this.state.stateFilter, this.state.regionFilter, this.sortFilter);
    this.setState({pantries: data.results, numPantries: data.count});
  }

  async handlePageChange(pageNumber) {
    const data = await getSearchPantriesPage(pageNumber, this.state.searchEntry, this.state.stateFilter, this.state.regionFilter, this.state.sortFilter);
    this.setState({pantries: data.results, activePage: pageNumber});
    window.scrollTo(0, 0)
  }

  async updateSearch(entry) {
    const data = await getSearchPantriesPage(1, entry, this.state.stateFilter, this.state.regionFilter, this.state.sortFilter);
    this.setState({pantries: data.results, numPantries: data.count, activePage: 1, searchEntry: entry});
    window.scrollTo(0, 0)
  }

  async updateSort(sort) {
    const data = await getSearchPantriesPage(1, this.state.searchEntry, this.state.stateFilter, this.state.regionFilter, sort);
    this.setState({pantries: data.results, numPantries: data.count, activePage: 1, sortFilter: sort});
    window.scrollTo(0, 0)      
  }

  async updateState(state) {
    const data = await getSearchPantriesPage(1, this.state.searchEntry, state, this.state.regionFilter, this.state.sortFilter);
    this.setState({pantries: data.results, numPantries: data.count, activePage: 1, stateFilter: state});
    window.scrollTo(0, 0)
  }

  async reset() {
    const data = await getSearchPantriesPage(1, '', '', '', '');
    this.setState({pantries: data.results, numPantries: data.count, activePage: 1, searchEntry: '', sortFilter: '', regionFilter: '', stateFilter: ''});
    window.scrollTo(0, 0)
  }

  options = [["Name: A-Z", "Name: Z-A"], ["Alabama", "Alaska", "Arizona", "Arkansas", "California", "Colorado", "Connecticut",
              "Delaware", "District of Columbia", "Florida", "Georgia", "Hawaii", "Idaho", "Illinois", "Indiana", "Iowa", "Kansas", "Kentucky", "Louisiana", "Maine", "Maryland",
              "Massachusetts", "Michigan", "Minnesota", "Mississippi", "Missouri", "Montana", "Nebraska", "Nevada", "New Hampshire", "New Jersey", "New Mexico", "New York",
              "North Carolina", "North Dakota", "Ohio", "Oklahoma", "Oregon", "Pennsylvania", "Rhode Island", "South Carolina", "South Dakota", "Tennessee", "Texas", 
              "Utah", "Vermont", "Virginia", "Washington", "West Virginia", "Wisconsin", "Wyoming"]]

  dict = {"Name: A-Z": "alphaASC", "Name: Z-A": "alphaDESC", "Northeast": "Northeast", "Midwest": "Midwest", "South": "South", "West":"West",
          "Alabama": "Alabama", "Alaska": "Alaska", "Arizona": "Arizona", "Arkansas": "Arkansas", "California": "California", "Colorado": "Colorado", 
          "Connecticut": "Connecticut", "Delaware": "Delaware", "District of Columbia": "District of Columbia", "Florida": "Florida", "Georgia": "Georgia",
          "Hawaii": "Hawaii", "Idaho": "Idaho", "Illinois": "Illinois", "Indiana" : "Indiana", "Iowa": "Iowa", "Kansas": "Kansas", "Kentucky": "Kentucky",
          "Louisiana": "Louisiana", "Maine": "Maine", "Maryland": "Maryland", "Massachusetts": "Massachusetts", "Michigan": "Michigan", "Minnesota": "Minnesota",
          "Mississippi":  "Mississippi", "Missouri": "Missouri", "Montana": "Montana", "Nebraska": "Nebraska", "Nevada": "Nevada", "New Hampshire": "New Hampshire",
          "New Jersey": "New Jersey", "New Mexico": "New Mexico", "New York": "New York", "North Carolina": "North Carolina", "North Dakota": "North Dakota", 
          "Ohio": "Ohio", "Oklahoma": "Oklahoma", "Oregon": "Oregon", "Pennsylvania": "Pennsylvania", "Rhode Island": "Rhode Island", "South Carolina": "South Carolina",
          "South Dakota":  "South Dakota", "Tennessee": "Tennessee", "Texas": "Texas", "Utah": "Utah", "Vermont": "Vermont", "Virginia": "Virginia", 
          "Washington": "Washington", "West Virginia": "West Virginia", "Wisconsin": "Wisconsin", "Wyoming": "Wyoming"}

  render() {
    return (
      <div className="FoodPantries"> 
        <MyNavBar />
        <Search placeholder="Search for a specific Food Pantry..." searchParam={["Sort", "Filter by State"]} 
        options={this.options}
        updateSearch={this.updateSearch} updateSort={this.updateSort}
        updateRegion={this.updateState} updateThird={this.updateState} 
        reset={this.reset}
        dict = {this.dict}
        />
        <Container>
          <Row className = "modelRow">
            {this.state.pantries.map(dynamicPantry => 
              <Col sm="3" className = "modelCol">
                <div key = {dynamicPantry.name}>
                  <MyCard 
                    title = {dynamicPantry.name} 
                    link = {"./foodpantries/" + dynamicPantry.id} 
                    image = {dynamicPantry.logo_img}
                    searchEntry = {this.state.searchEntry}
                    text1 = {"Poverty rate: "+dynamicPantry.state[1]}
                    text2 = {"City: "+dynamicPantry.city}
                    text3 = {"State: "+dynamicPantry.state[0]}/>
                </div>
              </Col>
            )}
          </Row>
        </Container>
        <Pagination className = "pagination"
          pageRangeDisplayed  = {10}
          activePage = {this.state.activePage}
          activeLinkClass = "active"
          itemsCountPerPage = {16}
          totalItemsCount = {this.state.numPantries}
          onChange = {this.handlePageChange}
        />
      </div>
    );
  }
}

export default FoodPantries;
