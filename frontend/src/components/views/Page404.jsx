import React, { Component } from 'react';
import MyNavBar from '../MyNavBar';
import MyFooter from '../MyFooter';
import "./Page404.css"
class Page404 extends Component {

    render() {

      return (
        <div className="Home">
          <MyNavBar />
          <div className="gif">
            <iframe src={"https://giphy.com/embed/l1J9EdzfOSgfyueLm"} 
                    width={1000} 
                    height={1000}
                    style={{position: "absolute"}}
                    frameBorder={0} 
                    class={"giphy-embed"} 
                    allowFullScreen>
            </iframe>
          </div>
        </div>
      );
    }
  }
  
  export default Page404;