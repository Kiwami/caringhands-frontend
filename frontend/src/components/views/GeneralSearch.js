import React, { Component } from 'react';
import './Models/Model.css';

import { TabContent, TabPane, Nav, NavItem, NavLink, Row, Col, Container } from 'reactstrap';
import classnames from 'classnames';


/* Layout components */
import MyNavBar from '../MyNavBar'
import MyCard   from '../MyCard'

/* Pagination components */
import Pagination from "react-js-pagination";
import { getSearchStatesPage } from '../../queries/stateQueries';
import { getSearchPantriesPage } from '../../queries/pantryQueries';
import { getSearchOpportunitiesPage } from '../../queries/volunteerQueries';

class GeneralSearch extends Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      s_instances: [],
      f_instances: [],
      v_instances: [],
      s_numInstances: 0,
      f_numInstances: 0,
      v_numInstances: 0,
      s_activePage: 1,
      f_activePage: 1,
      v_activePage: 1,
      searchEntry: this.props.match.params.id,
      activeTab: '1'
    };
    this.handlePageChange = this.handlePageChange.bind(this);
}

async toggle(tab) {
  if (this.state.activeTab !== tab) {
    this.setState({activeTab: tab}); 
  }
}

async componentDidMount() {
  const s_data = await getSearchStatesPage(1, this.state.searchEntry, '', '', '');
  this.setState({s_instances: s_data.results, s_numInstances: s_data.count});

  const f_data = await getSearchPantriesPage(1, this.state.searchEntry, '', '', '');
  this.setState({f_instances: f_data.results, f_numInstances: f_data.count});

  const v_data = await getSearchOpportunitiesPage(1, this.state.searchEntry, '', '', '');
  this.setState({v_instances: v_data.results, v_numInstances: v_data.count});
}

async handlePageChange(pageNumber) {
  var data;
  if(this.state.activeTab === '1') {
    data = await getSearchStatesPage(pageNumber, this.state.searchEntry, '', '', '');
    this.setState({s_instances: data.results, s_activePage: pageNumber});
  } else if (this.state.activeTab === '2') {
    data = await getSearchPantriesPage(pageNumber, this.state.searchEntry, '', '', '');
    this.setState({f_instances: data.results, f_activePage: pageNumber});
  } else if (this.state.activeTab === '3') {
    data = await getSearchOpportunitiesPage(pageNumber, this.state.searchEntry, '', '', '');
    this.setState({v_instances: data.results, v_activePage: pageNumber});
  }
  window.scrollTo(0, 0)
}


text = ["poverty rate: ", "abrv: ", "region: "]

  render() {

    var states = (<div className="General Search"> 
                  <Container className = "modelContainer">
                      <Row className = "modelRow">
                            {this.state.s_instances.map(dynamicState => 
                              <Col sm="3" className = "modelCol">
                                <div key = {dynamicState.name}>
                                  <MyCard 
                                    title = {dynamicState.name} 
                                    link = {"http://caringhands.me/states/" + dynamicState.name} 
                                    image = {dynamicState.main_img}
                                    searchEntry = {this.state.searchEntry}
                                    text1 = {"Poverty rate: "+dynamicState.poverty}
                                    text2 = {"Abrv: "+dynamicState.abrv}
                                    text3 = {"Region: "+dynamicState.region}/>
                                </div>
                              </Col>
                            )}
                      </Row>
                  </Container>
                  <Pagination className = "pagination"
                    pageRangeDisplayed  = {10}
                    activePage = {this.state.s_activePage}
                    activeLinkClass = "active"
                    itemsCountPerPage = {16}
                    totalItemsCount = {this.state.s_numInstances}
                    onChange = {this.handlePageChange}
                  />
                </div>);

    var foodpantries = (<div className="General Search"> 
                          <Container>
                          <Row className = "modelRow">
                            {this.state.f_instances.map(dynamicPantry => 
                              <Col sm="3" className = "modelCol">
                                <div key = {dynamicPantry.name}>
                                {console.log(dynamicPantry.state)}
                                  <MyCard 
                                    title = {dynamicPantry.name} 
                                    link = {"http://caringhands.me/foodpantries/" + dynamicPantry.id} 
                                    image = {dynamicPantry.logo_img}
                                    searchEntry = {this.state.searchEntry}
                                    text1 = {"Poverty rate: "+(typeof dynamicPantry.state === 'undefined' ?"":dynamicPantry.state[1])}
                                    text2 = {"City: "+dynamicPantry.city}
                                    text3 = {"State: "+(typeof dynamicPantry.state === 'undefined' ?"":dynamicPantry.state[0])}/>
                                </div>
                              </Col>
                            )}
                          </Row>
                        </Container>
                        <Pagination className = "pagination"
                          pageRangeDisplayed  = {10}
                          activePage = {this.state.f_activePage}
                          activeLinkClass = "active"
                          itemsCountPerPage = {16}
                          totalItemsCount = {this.state.f_numInstances}
                          onChange = {this.handlePageChange}
                        />
                        </div>);
    var volunteer = (
        <div className="General Search"> 
          <Container>
          <Row className = "modelRow">
            {this.state.v_instances.map(dynamicOpportunity => 
              <Col sm="3" className = "modelCol">
                <div key = {dynamicOpportunity.name}>
                  <MyCard 
                    title = {dynamicOpportunity.name} 
                    link = {"http://caringhands.me/volunteer/" + dynamicOpportunity.id} 
                    image = {dynamicOpportunity.logo_img}
                    searchEntry = {this.state.searchEntry}
                    text1 = {"Poverty rate: "+(typeof dynamicOpportunity.state === 'undefined' ?"":dynamicOpportunity.state[1])}
                    text2 = {"City: "+dynamicOpportunity.city}
                    text3 = {"State: "+(typeof dynamicOpportunity.state === 'undefined' ?"":dynamicOpportunity.state[0])}/>
                  </div>
                </Col>
              )}
          </Row>
        </Container>
        <Pagination className = "pagination"
          pageRangeDisplayed  = {10}
          activePage = {this.state.v_activePage}
          activeLinkClass = "active"
          itemsCountPerPage = {16}
          totalItemsCount = {this.state.v_numInstances}
          onChange = {this.handlePageChange}
        />
        </div>
      );

    return (
      <div>
          <MyNavBar />
          <h1>
            Search Results for: {this.props.match.params.id}
          </h1>

          <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
              onClick={() => { this.toggle('1'); }}
            >
              States
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '2' })}
              onClick={() => { this.toggle('2'); }}
            >
              Food Pantries
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '3' })}
              onClick={() => { this.toggle('3'); }}
            >
              Volunteer
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={this.state.activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
                {states}
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col sm="12">
                {foodpantries}
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="3">
            <Row>
              <Col sm="12">
                {volunteer}
              </Col>
            </Row>
          </TabPane>
        </TabContent>

      </div>
      );
    }
    
}

export default GeneralSearch;
