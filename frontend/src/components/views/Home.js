import React, { Component } from 'react';

import './Home.css';
import MyNavBar from '../MyNavBar'
import MyCarousel from '../MyCarousel'
import MyFooter from '../MyFooter'

class Home extends Component {
  render() {
    return (
      <div className="Home">
        <MyNavBar />
        <MyCarousel />
        <MyFooter />
      </div>
    );
  }
}

export default Home;
