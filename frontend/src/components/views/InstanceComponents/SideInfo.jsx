import React, { Component } from 'react';
import Circle from 'react-circle';

//css
import "./SideInfo.css"

class SideInfo extends Component {
    render() {
        let rate = this.props.rate;
        let img = this.props.img;
        return (
            <div>
                <h1> __________</h1>
                <h3>Poverty Rate:</h3>
                <Circle
                    animate={true} // Boolean: Animated/Static progress
                    animationDuration="1s" //String: Length of animation
                    responsive={true} // Boolean: Make SVG adapt to parent size
                    size={150} // Number: Defines the size of the circle.
                    lineWidth={14} // Number: Defines the thickness of the circle's stroke.
                    progress={rate} // Number: Update to change the progress and percentage.
                    progressColor="Crimson"  // String: Color of "progress" portion of circle.
                    bgColor="LightGrey" // String: Color of "empty" portion of circle.
                    textColor="Crimson" // String: Color of percentage text color.
                    textStyle={{
                        font: 'bold 5rem Helvetica, Arial, sans-serif' // CSSProperties: Custom styling for percentage.
                    }}
                    percentSpacing={10} // Number: Adjust spacing of "%" symbol and number.
                    roundedStroke={true} // Boolean: Rounded/Flat line ends
                    showPercentage={true} // Boolean: Show/hide percentage.
                    showPercentageSymbol={true} // Boolean: Show/hide only the "%" symbol.
                />
                <h3>Location:</h3>
                <img className="locationImg" src={img} alt='' />

            </div>
        );
    }
}

export default SideInfo