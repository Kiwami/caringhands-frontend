import React, { Component } from 'react';
import {Map, Marker, GoogleApiWrapper} from 'google-maps-react';


class GoogleMap extends Component {

  render() {
    return (
      // Important! Always set the container height explicitly
      <Map
        google={this.props.google}
        style={{ height: '50vh', width: '100%' }}
        initialCenter={this.props.center}
        zoom={this.props.zoom}
      >
      <Marker
        title={'hahah no one sees'}
        name={'I do not have a name' }
        position={this.props.center} />
      </Map>
    );
  }
}
export default GoogleApiWrapper({
  apiKey: "AIzaSyDWM8-fhIAJLf0Y2PeJQ8Os9PrbIuwnQZU"
})(GoogleMap)
