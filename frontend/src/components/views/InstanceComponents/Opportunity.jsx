import React, { Component } from 'react';
import CarouselSlider from "react-carousel-slider"
//css
import "./Opportunity.css"
// import { backendAPI }  from '../../../config'; 

import { Link } from 'react-router-dom'; 


class Opportunity extends Component {

    constructor(props) {
        super(props);
        this.state = {
          error: null,
          isLoaded: false,
          status: 0,
          items: []
        };
    }

    componentDidMount() {

        fetch(`http://api.caringhands.me/states/${this.props.param}/?format=json`, {
            method: "GET",
            headers: {
                "Content-Type": "application/json",
            }
        })
          .then(res => {
            this.setState({
              status: res.status
            });
            return res.json();
          })
          .then(
            (result) => {
              this.setState({
                isLoaded: true,
                items: result,
              });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
          )
    }

    render() {
        let itemsStyle = {
            padding: "0px",
            background: "white",
            margin:"0 10px",
            boxShadow: "1px 1px 1px 1px #9E9E9E",
            borderRadius: "4px"
        };

        
        let textBoxStyle = {
            width: "80%",
            top: "250px",
            color: "black",
            background: "transparent",
            fontSize: "14px",
            fontFamily: "Times New Roman"
        };

        let imgStyle = {
            width: "180px",
            height: "200px",
            borderBottom: "1px solid #9E9E9E",
            objectFit: "cover"
        };


        var { error, isLoaded, status, items } = this.state
        if (error) {
        return <div>Error: {error.message}</div>;
        } else if (!isLoaded){
            return <div>Loading...</div>
        }else if (status !== 200 || items.length === 0){
            return <div></div>
        }else{
            var prefix = ""
            if(this.props.type === 1){
                prefix = "/volunteer/"
            }else{
                prefix = "/foodpantries/"
            }
            let scientists = items.map( item => 
            <Link to = {prefix + item.id} key = {item.id} >
                <img style={imgStyle} src = {item.imgSrc} alt=''></img>
                <p style = {textBoxStyle} >{item.name}</p>
            </Link>
            );

            let btnWrapperStyle = {
                position: "relative",
                borderRadius: "50%",
                height: "50px",
                width: "50px",
                boxShadow: "1px 1px 1px 1px #9E9E9E",
                textAlign: "center"
            }
            /*
            let manner = {
                autoSliding: {interval: "3s"},
                duration: "2s"
            };
            */
            let btnStyle = {
                display: "inline-block",
                position: "relative",
                top: "50%",
                transform: "translateY(-50%)",
                fontSize: "36px"
            }

            let rBtnCpnt = (<div style = {btnWrapperStyle} >
                <div style = {btnStyle} className = "material-icons" >chevron_right</div>
            </div>);

            let lBtnCpnt = (<div style = {btnWrapperStyle} >
                <div style = {btnStyle} className = "material-icons" >chevron_left</div>
            </div>);        
            
            let scientistsCard = (<CarouselSlider 
                sliderBoxStyle = {{height: "400px", width: "80%", background: "transparent"}} 
                accEle = {{dots: false}}
                slideCpnts = {scientists} 
                itemsStyle = {itemsStyle} 
                buttonSetting = {{placeOn: 'middle-outside'}}
                //manner = {manner} 
                rBtnCpnt = {rBtnCpnt}
                lBtnCpnt = {lBtnCpnt}
            />);

            return (
                <div style = {{position:"relative", margin: "0 auto", width: "100%"}} >
                    <h3>{this.props.title}: </h3>
                    {scientistsCard}
                </div>
            );
        }
    }
}

export default Opportunity