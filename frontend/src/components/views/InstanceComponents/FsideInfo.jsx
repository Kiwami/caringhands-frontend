import React, { Component } from 'react';
import { Link } from 'react-router-dom'; 
//css
import "./SideInfo.css"

//google map
import GoogleMap from "./GoogleMap";

class SideInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
          error: null,
          isLoaded: false,
          status: 0,
          item: null
        };
    }

    componentDidMount() {
        fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${this.props.location}&key=AIzaSyCB-pnayJCIwN1b3AOIXfvY_rAN9CjmjUU`)
          .then(res => {
            this.setState({
              status: res.status
            });
            return res.json();
          })
          .then(
            (result) => {
              this.setState({
                isLoaded: true,
                item: result,
              });
            },
            // Note: it's important to handle errors here
            // instead of a catch() block so that we don't swallow
            // exceptions from actual bugs in components.
            (error) => {
              this.setState({
                isLoaded: true,
                error
              });
            }
        )
    }

    render() {
        var { error, isLoaded, status, item } = this.state
        if (error) {
        return <div>Error: {error.message}</div>;
        } else if (!isLoaded){
            return <div>Loading...</div>
        }else if(status!==200){
            return <h1>404, page not found</h1>
        }else{
            let lat = item.results[0].geometry.location.lat
            let lng = item.results[0].geometry.location.lng
            let center = {lat: lat, lng: lng}
            let logo = this.props.logo;
            let link = this.props.link;
            let location = this.props.location
            // let phone = this.props.phone
            let state = this.props.state
            let pos = this.props.pos
            return (
                <div className="FSideInfo">
                    <h1> __________</h1>
                    <img className="logoImg" src={logo} alt=''/>
                    <h3>Website:</h3>
                    <a href={link}>{link}</a>
                    <br/>
                    {/* <h3>Contact:</h3>
                    <p>{phone}</p> */}
                    <h3>State: </h3>
                    <p>{state}</p>
                    <Link to = {'/states/' + state} key = {state} >
                        <img className="locationImg" src={pos} alt=''/>
                    </Link>
                    <br/>
                    <h3>Location:</h3>
                    <p>{location}</p>
                    <GoogleMap center={center} zoom={15}/>
                </div>
            );
        }
    }
}

export default SideInfo