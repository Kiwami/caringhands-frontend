import React, { Component } from 'react';

//CSS
import "./MainDescription.css"

class MainDescription extends Component {
    
    render() {
        let name = this.props.name;
        let description = this.props.description;
        let img = this.props.img;
        console.log(img)
        return (
            <div>
                <h1>{name}</h1>
                <img className="instance_image"  src={img} alt=''/>
                <hr />
                <h3>Description:</h3>
                <p>
                    {description}
                </p>
            </div>
        );
        
    };
}

export default MainDescription
