import React from 'react';
import { Col, Row, Button, Input } from 'reactstrap';
import SearchButton from './SearchButton';

export default class Search extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.resetSearchValue = this.resetSearchValue.bind(this);
    this.buttonSearch = this.buttonSearch.bind(this);
    this.searchDropdown = [];

    this.state = {
        dropdownOpen: false,
        searchValue: ''
    }; 
  }

  toggle() {
    this.setState({
      dropdownOpen: !this.state.dropdownOpen
    });
  } 

  updateSearchValue(event) {
    this.setState({
      searchValue: event.target.value
    });
  }

  resetSearchValue(event) {
    for(var i = 0;i < this.props.searchParam.length; i++) {
      this.searchDropdown[i].reset();
    }
    this.setState({
      searchValue: ''
    });
    this.props.reset();
  }

  buttonSearch() {
    this.props.updateSearch(this.state.searchValue);
  }


  render() {
    return (
      <div className="searchBar">
        <Row className="searchRow" form>
          <Button classname="resetButton" size="xs" color="danger" onClick={this.resetSearchValue}> Reset </Button>
          {this.props.searchParam.map( (dynamicParam, index) => 
            <div>
              <SearchButton index = {index} 
              updateFunctions={[this.props.updateSort, this.props.updateRegion, this.props.updateThird]} 
              searchParam={dynamicParam} options={this.props.options[index]} ref={(el) => this.searchDropdown.push(el)}
              dict = {this.props.dict}/>
            </div>
          )}
          <Col md={3}>
            <Input type="text" id="searchInput" 
                    placeholder={this.props.placeholder} 
                    value={this.state.searchValue} 
                    onChange={event => this.updateSearchValue(event)}
                    onKeyPress={event => {
                      if (event.key === "Enter") {
                        this.props.updateSearch(this.state.searchValue);
                      }
                    }}/>
          </Col>
          <Button classname="searchButton" size="xs" onClick={this.buttonSearch} > Search </Button>            
        </Row>
        
      </div>
    );
  }
}
