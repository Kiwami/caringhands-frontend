import React from 'react';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Input} from 'reactstrap';

export default class MyNavBar extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      isOpen: false,
      searchValue: ''
    };
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }

  updateSearchValue(event) {
    this.setState({
      searchValue: event.target.value
    });
  }

  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
          <NavbarBrand href="/">Caring Hands</NavbarBrand>
          <NavbarToggler onClick={this.toggle} />
          <Collapse isOpen={this.state.isOpen} navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink href="/">Home</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/states">States</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/foodpantries">Food Pantries</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/volunteer">Volunteer</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/visualization">Visualization</NavLink>
              </NavItem>
              <NavItem>
                <NavLink href="/about">About</NavLink>
              </NavItem>
              <NavItem>
              <Input type="text" id="searchInput" 
                    placeholder="Search..." 
                    value={this.state.searchValue} 
                    onChange={event => this.updateSearchValue(event)}
                    onKeyPress={event => {
                    if (event.key === "Enter") {
                        if(this.state.searchValue !== '') {
                          window.location.assign('/search/' + this.state.searchValue);
                        } 
                    }
              }}/>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    );
  }
}