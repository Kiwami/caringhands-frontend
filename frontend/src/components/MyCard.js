import React, { Component } from 'react';
import { Card, CardImg, CardText, CardBody,
  CardTitle, CardSubtitle, Button, NavLink} from 'reactstrap';
import { BrowserRouter as Link} from 'react-router-dom'; 

import Highlighter from "react-highlight-words";

import './MyCard.css';

/* Highlight */
const highlightStyle = { backgroundColor: 'white', color: 'rgb(226, 121, 23)', padding: '0px'}

class MyCard extends Component {
  render() {
    return (
      <div className = "model">
        <Link to = {{pathname: this.props.link}} >
          <Card className="ModelCard">
            <CardImg className= "MyCardImage" top width="10%" src={this.props.image} alt="Card image cap"/>
            <CardBody>
              <CardTitle className = "modelTitle"> 
                <Highlighter
                  highlightClassName="YourHighlightClass"
                  highlightStyle={highlightStyle}
                  searchWords={[this.props.searchEntry]}
                  autoEscape={true}
                  textToHighlight={this.props.title}
                /> 
              </CardTitle>
              <CardSubtitle> {this.props.subtitle} </CardSubtitle>
              <CardText>
               <Highlighter
                   highlightClassName="YourHighlightClass"
                   highlightStyle={highlightStyle}
                   searchWords={[this.props.searchEntry]}
                   autoEscape={true}
                   textToHighlight={this.props.text1}
                 />
             </CardText>
             <CardText>
               <Highlighter
                   highlightClassName="YourHighlightClass"
                   highlightStyle={highlightStyle}
                   searchWords={[this.props.searchEntry]}
                   autoEscape={true}
                   textToHighlight={this.props.text2}
                 />
             </CardText>
             <CardText>
               <Highlighter
                   highlightClassName="YourHighlightClass"
                   highlightStyle={highlightStyle}
                   searchWords={[this.props.searchEntry]}
                   autoEscape={true}
                   textToHighlight={this.props.text3}
                 />
             </CardText>
              <Button className = "button" color = "info">
                <NavLink className = "inactive" href={this.props.link}>About</NavLink>
              </Button>
            </CardBody>
          </Card>
        </Link>
      </div>
    );
  };
};

export default MyCard;
