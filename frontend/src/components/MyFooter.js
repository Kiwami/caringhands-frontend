import React from 'react';
import {
  Navbar,
  Nav,
  NavItem,
  NavLink} from 'reactstrap';

export default class MyNavBar extends React.Component {
  render() {
    return (
      <div>
        <Navbar color="light" light expand="md">
            <Nav className="ml-auto" navbar>
            <NavItem>
            <NavLink disabled href="#">Copyright © CaringHands 2018</NavLink>
          </NavItem>
            </Nav>
        </Navbar>
      </div>
    );
  }
}