import React, { Component } from 'react';
import { Link } from 'react-router-dom'
import States_1 from '../data/States_1.jpg';
import Food_1 from '../data/Food_1.jpg';
import Volunteer_1 from '../data/Volunteer_1.jpg';
import './MyCarousel.css';
import {
  Carousel,
  CarouselItem,
  CarouselControl,
  CarouselIndicators,
  CarouselCaption
} from 'reactstrap';
//
const items = [
  {
    src: States_1,
    altText:<Link style={{ color: 'white' }} to="/states">States</Link>,
    caption: 'Learn more about each state and their poverty statistics'
  },
  {
    src: Food_1,
    altText: <Link style={{ color: 'white' }} to="/foodpantries">Food Pantries</Link>,
    caption: 'Learn about different food pantries near you'
  },
  {
    src: Volunteer_1,
    altText: <Link style={{ color: 'white' }} to="/volunteer">Volunteer</Link>,
    caption: 'Learn about different volunteer opportunities near you'
  }
];


class MyCarousel extends Component {
  constructor(props) {
    super(props);
    this.state = { activeIndex: 0 };
    this.next = this.next.bind(this);
    this.previous = this.previous.bind(this);
    this.goToIndex = this.goToIndex.bind(this);
    this.onExiting = this.onExiting.bind(this);
    this.onExited = this.onExited.bind(this);
  }

  onExiting() {
    this.animating = true;
  }

  onExited() {
    this.animating = false;
  }

  next() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === items.length - 1 ? 0 : this.state.activeIndex + 1;
    this.setState({ activeIndex: nextIndex });
  }

  previous() {
    if (this.animating) return;
    const nextIndex = this.state.activeIndex === 0 ? items.length - 1 : this.state.activeIndex - 1;
    this.setState({ activeIndex: nextIndex });
  }

  goToIndex(newIndex) {
    if (this.animating) return;
    this.setState({ activeIndex: newIndex });
  }

  render() {
    const { activeIndex } = this.state;

    const slides = items.map((item) => {
      return (
        <CarouselItem
          onExiting={this.onExiting}
          onExited={this.onExited}
          key={item.src}
        >
          <img src={item.src} alt={item.altText} style = {{height: 600, width: 1300, aspectRatio: 1}}/>
          <CarouselCaption captionText={item.caption} captionHeader={item.altText} />
        </CarouselItem>
      );
    });

    return (
      <Carousel
        activeIndex={activeIndex}
        next={this.next}
        previous={this.previous}
      >
        <CarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={this.goToIndex} />
        {slides}
        <CarouselControl direction="prev" directionText="Previous" onClickHandler={this.previous} />
        <CarouselControl direction="next" directionText="Next" onClickHandler={this.next} />
      </Carousel>
    );
  }
}


export default MyCarousel;