import React from 'react';
import { ButtonDropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';

export default class SearchButton extends React.Component {
    constructor(props) {
      super(props);
  
      this.toggle = this.toggle.bind(this);
      this.select = this.select.bind(this);
      this.reset = this.reset.bind(this);
      this.state = {
          dropdownOpen: false,
          searchDefault: this.props.searchParam,
          searchTitle : this.props.searchParam
      }; 
    }

    toggle() {
        this.setState({
          dropdownOpen: !this.state.dropdownOpen
        });
    } 

    select(event) {
      
      this.setState({
        dropdownOpen: !this.state.dropdownOpen,
        searchTitle: event.target.innerText
      });
      
      this.props.updateFunctions[this.props.index](this.props.dict[event.target.innerText])
    }

    reset() {
      this.setState({
        searchTitle: this.state.searchDefault
      });
    }
    
      render() {
        return (
          <div>
              <ButtonDropdown className="searchButton" isOpen={this.state.dropdownOpen} toggle={this.toggle}>
              <DropdownToggle caret>
                {this.state.searchTitle}
              </DropdownToggle>
              <DropdownMenu
                modifiers={{
                  setMaxHeight: {
                    enabled: true,
                    order: 890,
                    fn: (data) => {
                      return {
                        ...data,
                        styles: {
                          ...data.styles,
                          overflow: 'auto',
                          maxHeight: 400,
                        },
                      };
                    },
                  },
                }}
              >
                {this.props.options.map( (dynamicOption) => 
                  <div>
                    <DropdownItem onClick={this.select}> {dynamicOption}  </DropdownItem>
                    <DropdownItem divider />
                  </div>
                )}
              </DropdownMenu>
              </ButtonDropdown>
          </div>
        );
      }
    }