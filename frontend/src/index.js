import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import Home from './components/views/Home';
import About from './components/views/AboutPage/About';
import States from './components/views/Models/States';
import FoodPantries from './components/views/Models/FoodPantries';
import Volunteer from './components/views/Models/Volunteer';
import Visualization from './components/views/Visualizations/Visualization';
//Instance Component
import StateInstance from './components/views/Instances/StateInstance';
import FoodInstance from './components/views/Instances/FoodInstance';
import VolunteerInstance from './components/views/Instances/VolunteerInstance';
//CSS
import 'bootstrap/dist/css/bootstrap.min.css';
// Search Page
import GeneralSearch from './components/views/GeneralSearch';


import { BrowserRouter as Router, Route } from "react-router-dom";


document.body.style.background = "#f3f3f3"
ReactDOM.render((
  <Router>
    <div>
      <Route exact path='/' component={Home}/>
      <Route exact path='/about' component={About}/>
      <Route exact path='/states' component={States}/>
      <Route exact path='/foodpantries' component={FoodPantries}/>
      <Route exact path='/volunteer' component={Volunteer}/>
      <Route exact path='/visualization' component={Visualization}/>

      <Route path="/states/:id" component={StateInstance} />
      <Route path="/foodpantries/:id" component={FoodInstance} />
      <Route path="/volunteer/:id" component={VolunteerInstance} />
      <Route path='/search/:id' component={GeneralSearch} />
    </div>
  </Router>
  ), document.getElementById('root'))
