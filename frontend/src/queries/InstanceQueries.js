import { backendAPI }  from '../config.js'
import axios from 'axios';

export async function getStateInfo(id) {
    const response = await axios.get(backendAPI + `states/${id}/?format=json`);
        return response.data;
}

export async function getVolunteerInfo(id) {
    const response = await axios.get(backendAPI + `volunteer/${id}/?format=json`);
        return response.data;
}

export async function getFoodpantriesInfo(id) {
    const response = await axios.get(backendAPI + `foodpantries/${id}/?format=json`);
        return response.data;
}