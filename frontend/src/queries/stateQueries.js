import { backendAPI }  from '../config.js'
import axios from 'axios';

export async function getSearchStatesPage(pageNumber, searchTerm, poverty, region, sortBy) {
    const response = await axios.get(`${backendAPI}states/?format=json&page=${pageNumber}&poverty=${poverty}&region=${region}&sort=${sortBy}&search=${searchTerm}`);
    return response.data;
}
