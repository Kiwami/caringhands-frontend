import { backendAPI }  from '../config.js'
import axios from 'axios';

export async function getSearchPantriesPage(pageNumber, searchTerm, state, region, sortBy) {
    const response = await axios.get(`${backendAPI}foodpantries/?format=json&page=${pageNumber}&state=${state}&region=${region}&sort=${sortBy}&search=${searchTerm}`);
    return response.data;
}
  