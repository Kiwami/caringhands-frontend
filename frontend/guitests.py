import os
import requests
import http.client
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from unittest import main, TestCase

class TestCases(TestCase):
    def setUp(self):
        chromeOpts = webdriver.ChromeOptions()
        chromedriver = "./chromedriver"
        chromeOpts.add_argument('--headless')
        chromeOpts.add_argument('--disable-gpu')
        chromeOpts.add_argument('--no-sandbox')
        self.driver = webdriver.Chrome(executable_path= chromedriver, options=chromeOpts)


    def test_title(self):
        self.driver.get("http://caringhands.me")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_title_states(self):
        self.driver.get("http://caringhands.me/states")
        self.assertEqual(self.driver.title, 'Caring Hands')
    
    def test_title_food_pantries(self):
        self.driver.get("http://caringhands.me/foodpantries")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_title_volunteer(self):
        self.driver.get("http://caringhands.me/volunteer")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_title_about_us(self):
        self.driver.get("http://caringhands.me/about")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_title_Alabama(self):
        self.driver.get("http://caringhands.me/states/Alabama")
        self.assertEqual(self.driver.title, 'Caring Hands')
    
    def test_title_Alaska(self):
        self.driver.get("http://caringhands.me/states/Alaska")
        self.assertEqual(self.driver.title, 'Caring Hands')
    

    def test_title_Arizona(self):
        self.driver.get("http://caringhands.me/states/Arizona")
        self.assertEqual(self.driver.title, 'Caring Hands')
    

    def test_title_Arkansas(self):
        self.driver.get("http://caringhands.me/states/Arkansas")
        self.assertEqual(self.driver.title, 'Caring Hands')
    

    def test_title_California(self):
        self.driver.get("http://caringhands.me/states/California")
        self.assertEqual(self.driver.title, 'Caring Hands')
    

    def test_title_Colorado(self):
        self.driver.get("http://caringhands.me/states/Colorado")
        self.assertEqual(self.driver.title, 'Caring Hands')
    

    def test_title_Connecticut(self):
        self.driver.get("http://caringhands.me/states/Connecticut")
        self.assertEqual(self.driver.title, 'Caring Hands')
    

    def test_title_Delaware(self):
        self.driver.get("http://caringhands.me/states/Delaware")
        self.assertEqual(self.driver.title, 'Caring Hands')
    

    def test_title_Florida(self):
        self.driver.get("http://caringhands.me/states/Florida")
        self.assertEqual(self.driver.title, 'Caring Hands')
    
    def test_title_Georgia(self):
        self.driver.get("http://caringhands.me/states/Georgia")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_foodpantries_1(self):
        self.driver.get("http://caringhands.me/foodpantries/1")
        self.assertEqual(self.driver.title, 'Caring Hands')
    
    def test_foodpantries_2(self):
        self.driver.get("http://caringhands.me/foodpantries/2")
        self.assertEqual(self.driver.title, 'Caring Hands')
    
    def test_foodpantries_3(self):
        self.driver.get("http://caringhands.me/foodpantries/3")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_foodpantries_4(self):
        self.driver.get("http://caringhands.me/foodpantries/4")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_foodpantries_5(self):
        self.driver.get("http://caringhands.me/foodpantries/5")
        self.assertEqual(self.driver.title, 'Caring Hands')
    
    def test_foodpantries_6(self):
        self.driver.get("http://caringhands.me/foodpantries/6")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_foodpantries_7(self):
        self.driver.get("http://caringhands.me/foodpantries/7")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_foodpantries_8(self):
        self.driver.get("http://caringhands.me/foodpantries/8")
        self.assertEqual(self.driver.title, 'Caring Hands')
    
    def test_foodpantries_9(self):
        self.driver.get("http://caringhands.me/foodpantries/9")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_foodpantries_10(self):
        self.driver.get("http://caringhands.me/foodpantries/10")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_volunteer_1(self):
        self.driver.get("http://caringhands.me/volunteer/1")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_volunteer_2(self):
        self.driver.get("http://caringhands.me/volunteer/2")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_volunteer_3(self):
        self.driver.get("http://caringhands.me/volunteer/3")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_volunteer_4(self):
        self.driver.get("http://caringhands.me/volunteer/4")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_volunteer_5(self):
        self.driver.get("http://caringhands.me/volunteer/5")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_volunteer_6(self):
        self.driver.get("http://caringhands.me/volunteer/6")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_volunteer_7(self):
        self.driver.get("http://caringhands.me/volunteer/7")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_volunteer_8(self):
        self.driver.get("http://caringhands.me/volunteer/8")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_volunteer_9(self):
        self.driver.get("http://caringhands.me/volunteer/9")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_volunteer_10(self):
        self.driver.get("http://caringhands.me/volunteer/10")
        self.assertEqual(self.driver.title, 'Caring Hands')

    def test_foodpantries_all_links(self):
        #31938
        for x in range (1,31939):
            link = "http://caringhands.me/foodpantries/" + str(x)
            print(link)
            self.driver.get(link)
            self.assertEqual(self.driver.title, 'Caring Hands')

    # def test_volunteer_all_links(self):
    #     #10143
    #     for x in range (1,10144):
    #         link = "http://caringhands.me/volunteer/" + str(x)
    #         print(link)
    #         self.driver.get(link)
    #         self.assertEqual(self.driver.title, 'Caring Hands')

    
    def tearDown(self):
        self.driver.quit()

if __name__ == "__main__": 
    main()
    
